﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject bullet;
    public int health = 5; //set enemy health in editor
    float fireRate;
    float nextFire;
    private Rigidbody2D rb;

    PlayerController target;

    private Vector2 movement;

    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        fireRate = 1f;
        nextFire = Time.time;

        rb = this.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        CheckIfTimeToFire();

       

      
    }

    private void FixedUpdate()
    {
        moveEnemy(movement);
    }

    void moveEnemy(Vector2 direction)
    {
        //rb.MovePosition((Vector2)transform.position + (direction * Time.deltaTime * 2f));
    }

    void CheckIfTimeToFire()
    {
        if (Time.time > nextFire)
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
            nextFire = Time.time + fireRate;
        }
    }
    public void TakeDamage(int damageAmount) //called to damage the enemy
    {
        health -= damageAmount; //subtract from total current health

        if (health <= 0) //if health is 0
        {
            Destroy(gameObject); //kill the enemy
        }
    }

}
