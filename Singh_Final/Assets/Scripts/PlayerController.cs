﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    private Transform aimTransform;
    private Rigidbody2D rb;
    public float speed = 5f;
    private Rigidbody2D body;
    public GameObject arrowPrefab;
    public int health = 100;
    public int currentHealth;

    public bool hasKey1;
    public bool hasKey2;
    public bool hasKey3;

    public bool win = false;

    public bool alive;

    public Animator animator;
    public SpriteRenderer spriteRenderer;

    private int stable = 0;

    public HealthBar healthBar;

    private void Start()
    {
        currentHealth = health;
        healthBar.SetMaxHealth(health);
        body = GetComponent<Rigidbody2D>();
        hasKey1 = false;
        hasKey2 = false;
        hasKey3 = false;

        alive = true;

        rb = this.GetComponent<Rigidbody2D>();
    }

    private void Awake()
    {
        aimTransform = transform.Find("Aim");
    }

    private void Update()
    {
        HandleAiming();
        Death();


        
        
        //transform.position = transform.position + movement * Time.deltaTime;

        //body.MovePosition(new Vector2((transform.position.x + movement.x * speed * Time.deltaTime), transform.position.y + movement.y * speed * Time.deltaTime));

        animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")));

        


        

    }

    private void FixedUpdate()
    {

        Vector2 movement = new Vector3(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed); //player movement up down left right
        rb.MovePosition((Vector2)rb.position + (movement * Time.deltaTime * 2.5f));


        if (movement.x > 0f)
        {
            spriteRenderer.flipX = false;
        }
        else if (movement.x < 0f)
        {
            spriteRenderer.flipX = true;
        }

    }

    private void HandleAiming() //aims and shoots based on mouse positoin when clicked
    {
        Vector3 mousePosition = UtilsClass.GetMouseWorldPosition(); 
        

        Vector3 aimDirection = (mousePosition - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg; //return angle in degrees
        aimTransform.eulerAngles = new Vector3(0, 0, angle);

        Vector2 shootingDirection = new Vector2(aimDirection.x, aimDirection.y);
        shootingDirection.Normalize();

        if (Input.GetMouseButtonDown(0))
        {
            GameObject bullet = Instantiate(arrowPrefab, transform.position, Quaternion.identity);
            bullet.GetComponent<Rigidbody2D>().velocity = shootingDirection * 5.0f; //gives the bullet velocity in the direction it was shot
            bullet.transform.Rotate(0.0f, 0.0f, angle); //makes the bullet face the direction it was fired in
            Destroy(bullet, 2.0f); //saves memory
        }
    }

    public void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Key1")){
            Destroy(col.gameObject);
            hasKey1 = true;

        }

        if (col.gameObject.CompareTag("Key2"))
        {
            Destroy(col.gameObject);
            hasKey2 = true;

        }

        if (col.gameObject.CompareTag("Key3"))
        {
            Destroy(col.gameObject);
            hasKey3 = true;

        }

        if (col.gameObject.CompareTag("Enemy"))
        {
            
            currentHealth -= 1;
            healthBar.SetHealth(currentHealth);

        }
    }

    public void Death()
    {
        Scene thisScene = SceneManager.GetActiveScene();
        if (currentHealth < 1)
        {
            Destroy(gameObject);
            SceneManager.LoadScene(thisScene.name);
            
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Win"))
        {
            win = true;
        }
    }



}
