﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject range;
    Transform player;
    public int health = 3; //set enemy health in editor
    private Rigidbody2D rb;
    private Vector2 movement;
    public float moveSpeed = 10f;
    bool seen = false;
    public Animator animator;

    private void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        rb.rotation = angle;
        direction = direction.normalized;
        movement = direction;

        
        if (Vector2.Distance(transform.position, player.position) < 3f)
        {
            //moveEnemy(movement);
            seen = true;
            animator.SetBool("found", true);
        }
    }

    void moveEnemy(Vector2 direction)
    {
        rb.MovePosition((Vector2)rb.position + (direction * moveSpeed * Time.deltaTime * 2.5f));
    }

    private void FixedUpdate()
    {
        if (seen == true)
        {
            moveEnemy(movement);
            
        }
        
    }

    public void TakeDamage(int damageAmount) //called to damage the enemy
    {
        health -= damageAmount; //subtract from total current health

        if (health <= 0) //if health is 0
        {
            Destroy(gameObject); //kill the enemy
        }
    }
}
