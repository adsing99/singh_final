﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Enemy") && !col.gameObject.CompareTag("Player"))
        {
            EnemyController eHealth = col.gameObject.GetComponent<EnemyController>(); //reference EnemyHealth script on the enemy hit
            Shooter sHealth = col.gameObject.GetComponent<Shooter>();

            if (eHealth != null)
                eHealth.TakeDamage(1); //do 1 damage to the enemy
            Destroy(gameObject); //destroy the object

            if (sHealth != null)
                sHealth.TakeDamage(1); //do 1 damage to the enemy
            Destroy(gameObject); //destroy the object
        }

        if (!col.gameObject.CompareTag("Enemy") && !col.gameObject.CompareTag("Player") && !col.gameObject.CompareTag("EnemyBullet"))
        {
            Destroy(gameObject);
        }

        
    }
    
        
    }

